-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local map = vim.keymap.set
map({ "i", "x", "n", "s" }, "<f1>", function()
  require("harpoon"):list():select(1)
end, { desc = "Harpoon file 1" })

map({ "i", "x", "n", "s" }, "<f2>", function()
  require("harpoon"):list():select(2)
end, { desc = "Harpoon file 2" })

map({ "i", "x", "n", "s" }, "<f3>", function()
  require("harpoon"):list():select(3)
end, { desc = "Harpoon file 3" })

map({ "i", "x", "n", "s" }, "<f4>", function()
  require("harpoon"):list():select(4)
end, { desc = "Harpoon file 4" })

map({ "i", "x", "n", "s" }, "<f5>", function()
  require("harpoon"):list():select(5)
end, { desc = "Harpoon file 5" })

map({ "i", "x", "n", "s" }, "<f6>", function()
  require("harpoon"):list():select(6)
end, { desc = "Harpoon file 6" })

map({ "i", "x", "n", "s" }, "<f7>", function()
  require("harpoon"):list():select(7)
end, { desc = "Harpoon file 7" })

map({ "i", "x", "n", "s" }, "<f8>", function()
  require("harpoon"):list():select(8)
end, { desc = "Harpoon file 8" })

-- Move Lines
map("n", "<A-down>", "<cmd>m .+1<cr>==", { desc = "Move Down" })
map("n", "<A-up>", "<cmd>m .-2<cr>==", { desc = "Move Up" })
map("i", "<A-down>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move Down" })
map("i", "<A-up>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move Up" })
map("v", "<A-down>", ":m '>+1<cr>gv=gv", { desc = "Move Down" })
map("v", "<A-up>", ":m '<-2<cr>gv=gv", { desc = "Move Up" })
