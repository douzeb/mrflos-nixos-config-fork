# NixOS config entry point
# import the hardware and configuration files specific to your machine
{
  imports =
    [
      ./machines/MacBookPro12.1-hardware-configuration.nix
      ./machines/MacBookPro12.1-configuration.nix
    ];
}