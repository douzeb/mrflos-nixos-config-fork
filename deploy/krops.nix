let
  krops = builtins.fetchGit {
    url = "https://cgit.krebsco.de/krops/";
  };
  lib = import "${krops}/lib";
  pkgs = import "${krops}/pkgs" {};

  source = name: lib.evalSource [
    {
      dotfiles.file  = toString ../dotfiles;
      machines.file = toString ../machines;
      modules.file = toString ../modules;
      nixos-config.symlink = "machines/${name}/configuration.nix";
      nixpkgs.git = {
        clean.exclude = ["/.version-suffix"];
        ref = "086b448a5d54fd117f4dc2dee55c9f0ff461bdc1";
        url = https://github.com/NixOS/nixpkgs;
        shallow = true;
      };
      #secrets.pass = {
      #  dir  = toString ./secrets";
      #  name = "${name}";
      #};
    }
  ];

  zoro = pkgs.krops.writeDeploy "deploy-zoro" {
    source = source "zoro";
    target = lib.mkTarget "optisseur@10.42.0.3:4222" // {
      extraOptions = [
        #"-o"
        #"ControlMaster=auto"
        #"-o"
        #"ControlPath=~/.ssh/master-%r@%h:%p"
        #"-o"
        #"ControlPersist=480m"
      ];
      sudo = true;
    };
  };

  sanji = pkgs.krops.writeDeploy "deploy-sanji" {
    source = source "sanji";
    target = "root@server02.mydomain.org";
  };

in {
  zoro = zoro;
  sanji = sanji;
  all = pkgs.writeScript "deploy-all-servers"
    (lib.concatStringsSep "\n" [ zoro sanji ]);
}

# ssh-add -t 4h ~/.ssh/id_ed25519
# nix-build ./krops.nix -A zoro && ./result
# nix-build ./krops.nix -A sanji && ./result
# nix-build ./krops.nix -A all && ./result
