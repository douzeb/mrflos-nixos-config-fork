{
  # Auto system update
  system.autoUpgrade = {
    enable = true;
  };

  nix.settings.experimental-features = [ "nix-command" ];

  # Automatic Garbage Collection
  nix.gc = {
    automatic = true;
    dates = "daily";
    options = "--delete-older-than 7d";
  };

  # TODO : find what is installing this, probably obsidian?
  nixpkgs.config.permittedInsecurePackages = [ "electron-25.9.0" ];
}
