# cli basic programs and configs that should be available on every server
{ config, pkgs, ... }:
{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    bat
    btop
    findutils
    fzf
    gnugrep
    git
    glances
    htop
    lazygit
    lsd
    magic-wormhole
    mc
    micro
    mosh
    neovim
    ripgrep
    tmux
    tree
    unzip
    wget
  ];
}
