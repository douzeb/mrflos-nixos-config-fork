# Fonts and reasonable font-rendering.
{ pkgs, ... }:

{
  fonts = {
    packages = with pkgs; [
      corefonts
      dejavu_fonts
      inter
      #jetbrains-mono
      (nerdfonts.override { fonts = [ "Iosevka" ]; })
      #noto-fonts-cjk
      #noto-fonts-emoji
    ];

    fontconfig = {
      hinting.enable = true;
      subpixel.lcdfilter = "light";

      defaultFonts = {
        monospace = [ "Iosevka" ];
      };
    };
  };
}
