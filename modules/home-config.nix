# Inject the right home-manager config for the machine.
{
  config,
  pkgs,
  lib,
  ...
}:
let
  plasma-manager = pkgs.fetchFromGitHub {
    owner = "pjones";
    repo = "plasma-manager";
    rev = "4e56cfeb95081a43cb49487d0996dc936d6201e5";
    sha256 = "sha256-LJ/lFEupLSi7xtUps234hhMk7ZdVLRoYeU7KiCFaoGw=";
  };
  startupScript = pkgs.pkgs.writeShellScriptBin "hyprland-start" ''
    ${pkgs.waybar}/bin/waybar &
    ${pkgs.swww}/bin/swww init &

    sleep 1

    ${pkgs.swww}/bin/swww img /home/mrflos/Nextcloud/Images/Wallpapers/chihiro014.jpg &
  '';
in
{
  # TODO can we automate the installation of home-manager ?
  # sudo nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz home-manager
  imports = [ <home-manager/nixos> ];

  # Let Home Manager install and manage itself.
  #programs.home-manager.enable = true;
  home-manager.useGlobalPkgs = true;
  users.users.mrflos = {
    isNormalUser = true;
    description = "mrflos";
    createHome = true;
    extraGroups = [
      "users"
      "docker"
      "libvirtd"
      "lxd"
      "networkmanager"
      "wheel"
    ];
    uid = 1000;
    shell = pkgs.zsh;
  };

  nix.settings.trusted-users = [ "mrflos" ];
  home-manager.backupFileExtension = "backup";
  home-manager.users.mrflos =
    { pkgs, ... }:
    {
      home.stateVersion = "23.05";
      home.packages = with pkgs; [
        gcc
        cmake
        ripgrep
        fd
        lua-language-server
        rust-analyzer-unwrapped
        php83Packages.composer
        nodejs_20
        swww
        grimblast
        swaylock
        waybar
        yarn
      ];

      imports = [
        (plasma-manager + "/modules")
        ./home-plasma.nix
      ];
      home.file = {
        "./.config/kitty/" = {
          source = ../dotfiles/kitty;
          recursive = true;
        };

        "./.config/nvim/" = {
          source = ../dotfiles/nvim;
          recursive = true;
        };

        "./.config/lazygit/" = {
          source = ../dotfiles/lazygit;
          recursive = true;
        };

        "./.config/tmux/" = {
          source = ../dotfiles/tmux;
          recursive = true;
        };
      };
      home.sessionVariables = {
        EDITOR = "nvim";
      };
      home.sessionPath = [
        "$HOME/.local/bin"
        "$HOME/go/bin"
      ];
      accounts.email.accounts = {
        "mrflos@chmok.net" = {
          realName = "Florian Schmitt";
          userName = "mrflos@chmok.net";
          address = "mrflos@chmok.net";

          primary = true;
          thunderbird = {
            enable = true;
          };
          imap = {
            host = "mail.infomaniak.com";
            port = 993;
          };

          smtp = {
            host = "mail.infomaniak.com";
            port = 465;
          };
        };
        "mrflos@yeswiki.pro" = {
          realName = "Florian Schmitt - Yeswiki.pro";
          userName = "mrflos@yeswiki.pro";
          address = "mrflos@yeswiki.pro";

          thunderbird = {
            enable = true;
          };
          imap = {
            host = "mail.infomaniak.com";
            port = 993;
          };

          smtp = {
            host = "mail.infomaniak.com";
            port = 465;
          };
        };
        "mrflos@mrflos.pw" = {
          realName = "Florian Schmitt";
          userName = "mrflos";
          address = "mrflos@mrflos.pw";

          primary = false;
          thunderbird = {
            enable = true;
          };
          imap = {
            host = "mrflos.pw";
            port = 993;
            #tls.enable = true;
            #tls.useStartTls = true;
          };

          smtp = {
            host = "mrflos.pw";
            port = 587;
            #tls.useStartTls = true;
          };
        };
        "contact@yeswiki.pro" = {
          realName = "YesWiki.pro";
          userName = "contact@yeswiki.pro";
          address = "contact@yeswiki.pro";

          primary = false;
          thunderbird = {
            enable = true;
          };
          imap = {
            host = "mail.infomaniak.com";
            port = 993;
          };

          smtp = {
            host = "mail.infomaniak.com";
            port = 465;
          };
        };
        "contact@yeswiki.net" = {
          realName = "YesWiki.net";
          userName = "contact@yeswiki.net";
          address = "contact@yeswiki.net";

          primary = false;
          thunderbird = {
            enable = true;
          };
          imap = {
            host = "mail.infomaniak.com";
            port = 993;
          };

          smtp = {
            host = "mail.infomaniak.com";
            port = 465;
          };
        };
      };

      programs = {
        direnv = {
          enable = true;
          enableZshIntegration = true;
          nix-direnv.enable = true;
        };

        git = {
          enable = true;
          userName = "Florian Schmitt";
          userEmail = "mrflos@gmail.com";
          aliases = {
            fa = "fetch --all --tags --prune --recurse-submodules --force";
          };
          extraConfig = {
            pull.rebase = true;
            init.defaultBranch = "main";
            core.editor = "nvim";
            core.fileMode = false;
          };
        };

        neovim = {
          defaultEditor = true;
          viAlias = true;
          vimAlias = true;
        };

        starship = {
          enable = true;
          settings = with builtins; fromTOML (readFile ../dotfiles/starship/starship.toml);
        };

        zsh = {
          enable = true;
          autosuggestion.enable = true;
          enableCompletion = true;
          shellAliases = {
            #tmux = "tmux -f ~/.config/tmux/tmux.conf attach || tmux -f ~/.config/tmux/tmux.conf new";
            bunx = "bun --bun x";
            g = "lazygit";
            kdenix = "nix run github:pjones/plasma-manager --extra-experimental-features nix-command --extra-experimental-features flakes > /home/mrflos/Developpements/nixos-config/modules/home-plasma.nix";
            icat = "kitty +kitten icat";
            nixedit = "vi /etc/nixos";
            nixupdate = "sudo nix-channel --update && sudo nixos-rebuild switch";
            nixclean = "sudo nix-env --delete-generations old --profile /nix/var/nix/profiles/system && sudo /nix/var/nix/profiles/system/bin/switch-to-configuration switch && sudo nix-store --gc";
            socks = "ssh -N -f -C -D 9090 mrflos@yunohost.yeswiki.net -p 6742";
            vi = "/run/current-system/sw/bin/nvim";
            vim = "/run/current-system/sw/bin/nvim";
            yeswiki-updater = "cd /home/mrflos/Developpements/yeswiki-installer/ && sudo php yeswiki-updater.php";
          };
          initExtra = ''
            export BUN_INSTALL="$HOME/.bun"
            export PHP_CS_FIXER_IGNORE_ENV=1
            export PATH="$HOME/.local/bin:$BUN_INSTALL/bin:$PATH"
            eval "$(atuin init zsh)"
            eval "$(zoxide init zsh)"
          '';
          oh-my-zsh = {
            enable = true;
            extraConfig = ''
              zstyle :omz:plugins:ssh-agent helper ksshaskpass
              zstyle :omz:plugins:ssh-agent agent-forwarding yes
              zstyle :omz:plugins:ssh-agent lazy yes
            '';
            plugins = [
              "git"
              "ssh-agent"
            ];
            #  theme = "robbyrussell";
          };
        };
      };

      services.ssh-agent.enable = true;
      wayland.windowManager.hyprland.enable = true;
      wayland.windowManager.hyprland.settings = {
        exec-once = ''${startupScript}/bin/hyprland-start'';
        "$mod" = "SUPER";

        # assign apps
        "$term" = "kitty";
        "$editor" = "nvim";
        "$file" = "dolphin";
        "$browser" = "firefox";

        bind =
          [
            ", Print, exec, grimblast copy area"
            # Window/Session actions
            "$mod, Q, killactive"
            "ALT, F4, killactive"
            "$mod, delete, exit" # kill hyperland session
            "$mod, W, togglefloating" # toggle the window on focus to float
            "$mod, G, togglegroup" # toggle the window on focus to float
            "ALT, return, fullscreen" # toggle the window on focus to fullscreen
            "$mod, L, exec, swaylock" # lock screen
            #  "$mod, backspace, exec, wlogout-launcher-hyprland 1" # logout menu
            "$CONTROL, ESCAPE, exec, systemctl-toggle --user waybar.service"

            # Application shortcuts
            "$mod, T, exec, $term" # open terminal
            "$mod, E, exec, $file" # open file manager
            "$mod, C, exec, $editor" # open vscode
            "$mod, F, exec, $browser" # open browser

            # Move focus with mainMod + arrow keys
            "$mod, left, movefocus, l"
            "$mod, right, movefocus, r"
            "$mod, up, movefocus, u"
            "$mod, down, movefocus, d"
            "ALT, Tab, movefocus, d"

            # Switch workspaces relative to the active workspace with mainMod + CTRL + [←→]
            "$mod CTRL, right, workspace, r+1"
            "$mod CTRL, left, workspace, r-1"

            # move to the first empty workspace instantly with mainMod + CTRL + [↓]
            "$mod CTRL, down, workspace, empty"

            # Move active window to a relative workspace with mainMod + CTRL + ALT + [←→]
            "$mod CTRL ALT, right, movetoworkspace, r+1"
            "$mod CTRL ALT, left, movetoworkspace, r-1"

            # Move active window around current workspace with mainMod + SHIFT + CTRL [←→↑↓]
            "$mod SHIFT $CONTROL, left, movewindow, l"
            "$mod SHIFT $CONTROL, right, movewindow, r"
            "$mod SHIFT $CONTROL, up, movewindow, u"
            "$mod SHIFT $CONTROL, down, movewindow, d"

            # Scroll through existing workspaces with mainMod + scroll
            "$mod, mouse_down, workspace, e+1"
            "$mod, mouse_up, workspace, e-1"

            # Special workspaces (scratchpad)
            "$mod ALT, S, movetoworkspacesilent, special"
            "$mod, S, togglespecialworkspace,"

            # Toggle Layout
            "$mod, J, togglesplit," # dwindle
          ]
          ++ (
            # workspaces
            # binds $mod + [shift +] {1..10} to [move to] workspace {1..10}
            builtins.concatLists (
              builtins.genList (
                x:
                let
                  ws =
                    let
                      c = (x + 1) / 10;
                    in
                    builtins.toString (x + 1 - (c * 10));
                in
                [
                  "$mod, ${ws}, workspace, ${toString (x + 1)}"
                  "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
                  "$mod ALT, ${ws}, movetoworkspacesilent, ${toString (x + 1)}" # Move window silently to workspace Super + Alt + [0-9]
                ]
              ) 10
            )
          );

        # Move/Resize windows with mainMod + LMB/RMB and dragging
        bindm = [
          "$mod, mouse:272, movewindow"
          "$mod, mouse:273, resizewindow"
        ];

        # Resize windows
        binde = [
          "$mod SHIFT, right, resizeactive, 30 0"
          "$mod SHIFT, left, resizeactive, -30 0"
          "$mod SHIFT, up, resizeactive, 0 -30"
          "$mod SHIFT, down, resizeactive, 0 30"
        ];
      };

    };
}
