# Configuration specifically for laptops that move around.
{ ... }:

{
  # Automatically detect location for redshift & so on ...
  services.geoclue2.enable = true;
  location.provider = "geoclue2";

  # Enable power-saving features.
  # TODO find which one to use 
  # You have set services.power-profiles-daemon.enable = true; which conflicts with services.tlp.enable = true;
  # services.tlp.enable = true;

  programs.light.enable = true;
}
