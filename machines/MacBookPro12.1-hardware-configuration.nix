# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:

{
  imports = [
    (modulesPath + "/hardware/network/broadcom-43xx.nix")
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "nvme"
    "usbhid"
    "usb_storage"
    "sd_mod"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.blacklistedKernelModules = [ ];
  boot.extraModulePackages = with config.boot.kernelPackages; [
    #rtl8192eu
  ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/d56411cf-7072-48a8-aab7-70fb73695e64";
    fsType = "ext4";
  };

  fileSystems."/boot/efi" = {
    device = "/dev/disk/by-uuid/37E1-D07F";
    fsType = "vfat";
  };
  fileSystems."/mnt/nas/music" = {
    device = "192.168.1.2:/volume1/music";
    options = [
      "nfsvers=4.2"
      "x-systemd.automount"
      "noauto"
      "rw"
      "relatime"
      "user"
      "acl"
      "defaults"
    ];
    fsType = "nfs";
  };

  services.rpcbind.enable = true; # needed for NFS
  systemd.mounts = [
    {
      type = "nfs";
      mountConfig = {
        Options = "defaults,acl,user,noauto,relatime,rw";
      };
      what = "192.168.1.2:/volume1/music";
      where = "/mnt/nas/music";
    }
  ];

  systemd.automounts = [
    {
      wantedBy = [ "multi-user.target" ];
      automountConfig = {
        TimeoutIdleSec = "600";
      };
      where = "/mnt/nas/music";
    }
  ];

  swapDevices = [ ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp3s0.useDHCP = lib.mkDefault true;

  #nixpkgs.config.allowBroken = true;
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  hardware.opengl.enable = true;
  hardware.opengl.driSupport = true;
  powerManagement = {
    # Enable gradually increasing/decreasing CPU frequency, rather than using
    # "powersave", which would keep CPU frequency at 0.8GHz.
    cpuFreqGovernor = lib.mkDefault "conservative";

    # brcmfmac being loaded during hibernation would not let a successful resume
    # https://bugzilla.kernel.org/show_bug.cgi?id=101681#c116.
    # Also brcmfmac could randomly crash on resume from sleep.
    powerUpCommands = lib.mkBefore "${pkgs.kmod}/bin/modprobe brcmfmac && ${pkgs.kmod}/bin/modprobe brcmfmac_wcc";
    powerDownCommands = lib.mkBefore "${pkgs.kmod}/bin/rmmod brcmfmac_wcc && ${pkgs.kmod}/bin/rmmod brcmfmac";
  };

  # USB subsystem wakes up MBP right after suspend unless we disable it.
  services.udev.extraRules = lib.mkDefault ''
    SUBSYSTEM=="pci", KERNEL=="0000:00:14.0", ATTR{power/wakeup}="disabled"
  '';

  services.logind.extraConfig = ''
    RuntimeDirectorySize=8G
    HandleLidSwitchDocked=ignore
  '';
}
