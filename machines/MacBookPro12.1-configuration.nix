# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ’nixos-help’).

{ config, pkgs, ... }:

{
  hardware.bluetooth.enable = true;

  # Additional configuration from github/jeremiehuchet/nixos-macbookpro
  boot.kernelParams = [
    "hid_apple.fnmode=1"
    "hid_apple.swap_fn_leftctrl=1"
    "hid_apple.iso_layout=0"
  ];
  boot.kernel.sysctl = {
    "vm.swappiness" = 10;
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.consoleMode = "0";
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.hostName = "luffy"; # Define your hostname.
  networking.extraHosts = ''
    127.0.0.1 yeswiki.test
    192.168.36.206 chmok.net
    fd72:51a6:d583:0:984a:cc56:cf75:dc6 chmok.net
  '';
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;
  networking.firewall = {
    enable = true;
    allowedTCPPorts = [
      80
      443
    ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Moscow";

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "fr_FR.UTF-8";
    LC_ALL = "fr_FR.UTF-8";
    LC_IDENTIFICATION = "fr_FR.UTF-8";
    LC_MEASUREMENT = "fr_FR.UTF-8";
    LC_MESSAGES = "fr_FR.UTF-8";
    LC_MONETARY = "fr_FR.UTF-8";
    LC_NAME = "fr_FR.UTF-8";
    LC_NUMERIC = "fr_FR.UTF-8";
    LC_PAPER = "fr_FR.UTF-8";
    LC_TELEPHONE = "fr_FR.UTF-8";
    LC_TIME = "fr_FR.UTF-8";
  };

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us,ru";
    xkb.variant = "mac";
  };

  imports = [
    ../modules/console.nix
    ../modules/desktop.nix
    ../modules/fonts.nix
    ../modules/home-config.nix
    ../modules/laptop.nix
    ../modules/local-dev.nix
    ../modules/nixos.nix
  ];
  services.eris-server = {
    enable = true;
    backends = [
      "badger+file:///var/eris?put&get" # local one on /var/eris
      # "coap+tcp://eris.example.com:5683?get" # an online one
    ];
    mountpoint = "/mnt/media/eris"; # useful to mount eris-fs folders
    listenHttp = ":8080"; # or "[::1]:8080"
    listenCoap = ":5683"; # or "[::1]:5683"
    decode = true; # for http access via https://127.0.0.1:8080/uri-res/N2R?urn:eris:...
    package = pkgs.eris-go; # default package
  };
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}
