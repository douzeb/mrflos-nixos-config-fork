-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
local opt = vim.opt
opt.scrolloff = 9
opt.wrap = true
opt.clipboard = "unnamedplus"
opt.list = false
opt.relativenumber = true
opt.cursorline = false
