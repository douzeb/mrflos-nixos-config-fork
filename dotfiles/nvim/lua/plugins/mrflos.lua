function ColorMyPencils(color)
  color = color or "rose-pine"
  vim.cmd.colorscheme(color)

  vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
  vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end

return {
  -- theme Rosé Pine
  {
    "rose-pine/neovim",
    name = "rose-pine",
    config = function()
      require("rose-pine").setup({
        disable_background = true,
        styles = {
          italic = false,
        },
      })

      vim.cmd("colorscheme rose-pine")

      ColorMyPencils()
    end,
  },

  -- Configure LazyVim to load theme
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "rose-pine",
    },
  },

  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        "bash",
        "css",
        "dockerfile",
        "go",
        "html",
        "javascript",
        "json",
        "lua",
        "make",
        "markdown",
        "markdown_inline",
        "nix",
        "php",
        "python",
        "query",
        "regex",
        "rust",
        "toml",
        "tsx",
        "typescript",
        "twig",
        "vim",
        "vue",
        "yaml",
      },
    },
  },
  -- Neotree plugin
  {
    "nvim-neo-tree/neo-tree.nvim",
    opts = {
      close_if_last_window = true,
      event_handlers = {
        {
          event = "file_opened",
          handler = function()
            vim.cmd([[Neotree close]])
          end,
        },
      },
      filesystem = {
        filtered_items = {
          visible = true,
        },
      },
      window = {
        position = "current",
      },
    },
  },
  {
    "neovim/nvim-lspconfig",
    opts = {
      intelephense = {
        environment = {
          includePaths = {
            "/home/mrflos/Developpements/yeswiki",
          },
        },
      },
    },
  },
  {
    "folke/noice.nvim",
    event = "VeryLazy",
    opts = {
      cmdline = {
        view = "cmdline",
      },
      presets = {
        bottom_search = true,
        command_palette = false,
      },
    },
  },
  {
    "nvim-telescope/telescope.nvim",
    opts = {
      defaults = {
        layout_strategy = "bottom_pane",
        layout_config = {
          prompt_position = "top",
          preview_cutoff = 5, -- Preview should always show (unless previewer = false)
          width = function(_, max_columns, _)
            return math.min(max_columns, 120)
          end,
          height = function(_, _, max_lines)
            return math.min(max_lines, 15)
          end,
        },
        sorting_strategy = "ascending",
        border = true,
        borderchars = {
          prompt = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
          results = { "─", "│", "─", "│", "├", "╮", "╯", "╰" },
          preview = { "─", "│", "─", "│", "╭", "┤", "╯", "╰" },
        },
      },
    },
  },
}
