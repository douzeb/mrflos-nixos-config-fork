#!/usr/bin/env bash

# ATTENTION : il faut etre membre de l'organisation YesWiki sur github pour pouvoir cloner les repos

#default debian packages (a décommenter si besoin)
#printf "\n================ Install script dependencies  ================\n"
#sudo apt-get update
#sudo apt-get install \
#  php-common php-curl php-gd php-json php-mysql php-xml php-mbstring php-zip php-json
#  git \
#  curl \
#  jq -y
#printf "================ End install script dependencies ============\n"

corebranch="doryphore-dev"
curdir="$HOME/Developpements" #${PWD}
repos=$(curl --request GET --url "https://api.github.com/orgs/yeswiki/repos" | jq '.[].full_name' | tr -d "\"" | sort)

# core
printf "\n================ Install YesWiki core ================\n"
[ ! -d $curdir/yeswiki ] && git clone git@github.com:YesWiki/yeswiki.git $curdir/yeswiki
cd $curdir/yeswiki
git pull --tags
git checkout $corebranch
composer install -o
mkdir -p $curdir/yeswiki/custom/themes/
mkdir -p $curdir/yeswiki/custom/templates/
cd $curdir
printf "================ End install YesWiki core ============\n"

# extensions
printf "\n================ Install YesWiki extensions ================\n"
extensionprefix="YesWiki/yeswiki-extension-"
for i in $repos; do
	if [[ ${i:0:26} == $extensionprefix ]]; then
		dir="${i/YesWiki\//}"
		shortdir="${i/"$extensionprefix"/}"
		[ ! -d $curdir/$dir ] && git clone git@github.com:$i.git $curdir/$dir
		[ -f $curdir/$dir/composer.json ] && cd $curdir/$dir && composer install -o
		cd $curdir
		ln -s $curdir/$dir $curdir/yeswiki/tools/$shortdir
	fi
done
printf "================ End install YesWiki extensions ============\n"

# themes
printf "\n================ Install YesWiki themes ================\n"
themeprefix="YesWiki/yeswiki-theme-"
for i in $repos; do
	if [[ ${i:0:22} == $themeprefix ]]; then
		dir="${i/YesWiki\//}"
		shortdir="${i/"$themeprefix"/}"
		[ ! -d $curdir/$dir ] && git clone git@github.com:$i.git $curdir/$dir
		cd $curdir
		ln -s $curdir/$dir $curdir/yeswiki/custom/themes/$shortdir
	fi
done
printf "================ End install YesWiki themes ============\n"

# forge mrflos
printf "\n================ Install YesWiki misc from forge.mrflos.pw ================\n"
dir="yeswiki-custom-mutualisation.cc"
[ ! -d $curdir/$dir ] && git clone ssh://forgejo@forge.mrflos.pw:4222/yeswiki/yeswiki-custom-mutualisation.cc.git $curdir/$dir
dir="yeswiki-custom-adultes-relais.org"
[ ! -d $curdir/$dir ] && git clone ssh://forgejo@forge.mrflos.pw:4222/yeswiki/yeswiki-custom-adultes-relais.org.git $curdir/$dir
dir="yeswiki-custom-reseau.s-mart.fr"
[ ! -d $curdir/$dir ] && git clone ssh://forgejo@forge.mrflos.pw:4222/yeswiki/yeswiki-custom-reseau.s-mart.fr.git $curdir/$dir
dir="yeswiki-installer"
[ ! -d $curdir/$dir ] && git clone ssh://forgejo@forge.mrflos.pw:4222/yeswiki/yeswiki-installer.git $curdir/$dir
dir="doc.yeswiki.pro"
[ ! -d $curdir/$dir ] && git clone ssh://forgejo@forge.mrflos.pw:4222/yeswiki/doc.yeswiki.pro.git $curdir/$dir
printf "================ End install YesWiki misc from forge.mrflos.pw ============\n"

# other projects
printf "\n================ Install other projects ================\n"
dir="framemo"
[ ! -d $curdir/$dir ] && git clone ssh://git@framagit.org/colibris/framemo.git
printf "================ End install other projects ============\n"
