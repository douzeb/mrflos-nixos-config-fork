# NixOS config files from mrflos

NixOS config files to make a perfect (for me) server or desktop environment and local dev environment. Should be modular (cf. modules folder).
Mostly all was stolen, I mean inspired by [tazjin's nix files from TVL's repo](https://code.tvl.fyi/tree/users/tazjin/)

## Installation and setup

On a fresh NixOS installation

- check out this repository, for example in your home `git clone https://code.mrflos.pw/mrflos/nixos-config.git ~/nixos-config` (ideally fork this repo and clone it with ssh to be able to commit your customs)
- copy the NixOS generated configuration to the `machines` folder `cp /etc/hardware-configuration.nix ~/nixos-config/machines/<machinename>-hardware-configuration.nix`
- copy the example configuration file in the `machines` folder `cp ~/nixos-config/machines/example-configuration.nix ~/nixos-config/machines/<machinename>-configuration.nix`
- edit your machine configuration file `~/nixos-config/machines/<machinename>-configuration.nix`, make changes and uncomment modules according to your needs for this machine
- copy the example configuration file `~/nixos-config/configuration.example.nix` to `~/nixos-config/configuration.nix`
- edit the main configuration file `~/nixos-config/configuration.nix` and point to the right files

```nix
# NixOS config entry point
# import the hardware and configuration files specific to your machine
{
  imports =
    [
      ./machines/<machinename>-hardware-configuration.nix
      ./machines/<machinename>-configuration.nix
    ];
}
```

- move the old `/etc/nixos` folder `sudo mv /etc/nixos /etc/nixos.old` and symlink your custom one `sudo ln -s ~/nixos-config /etc/nixos`
- run the install `sudo nixos-rebuild switch`
- if all good don't forget to commit your changes
