# Window Manager and desktop programs
{ lib, pkgs, ... }:
{
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;

  services = {
    openvpn.servers = {
      arn = {
        config = ''config /home/mrflos/Nextcloud/vpn\ ARN/2023/vpn510.conf '';
        autoStart = false;
      };
    };
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    printing.enable = true; # Enable CUPS to print documents.
    printing.drivers = [ pkgs.brlaser ];
    avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };

    xserver.desktopManager.plasma5.enable = true;
    displayManager.defaultSession = "plasmawayland";
    displayManager.sddm.wayland.enable = true;
    displayManager.sddm.enable = true;
    displayManager.sddm.enableHidpi = true;
    displayManager.sddm.theme = "rose-pine";
    #xserver = {
    #  enable = true;
    #  desktopManager.plasma5.enable = true;
    #};
    dbus.enable = true;
  };

  programs = {
    chromium = {
      enable = true;
      homepageLocation = "about:blank";

      extensions = [
        "pejkokffkapolfffcgbmdmhdelanoaih" # Unsplash instant
        "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
        "gfapcejdoghpoidkfodoiiffaaibpaem" # Dracula theme
        "jbbplnpkjmmeebjpijfedlgcdilocofh" # wave a11y
      ];

      extraOpts = {
        SpellcheckEnabled = true;
        SpellcheckLanguage = [
          "fr-FR"
          "en-GB"
          "ru"
        ];
      };
    };
    droidcam.enable = true;
    firefox.enable = true;
    firefox.languagePacks = [ "fr" ];
    #hyprland.enable = true;

    ssh.askPassword = pkgs.lib.mkForce "${pkgs.ksshaskpass.out}/bin/ksshaskpass"; # conflict between kde and gnome cf. https://github.com/NixOS/nixpkgs/issues/75867
    ssh.startAgent = true;

    steam.enable = true;

    nix-ld.enable = true;
    # Sets up all the libraries to load
    nix-ld.libraries = with pkgs; [
      stdenv.cc.cc
      zlib
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # no kde bloat
  environment.plasma5.excludePackages = with pkgs.libsForQt5; [
    baloo
    elisa
    khelpcenter
    oxygen
    plasma-browser-integration
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    (callPackage ./sddm-rose-pine.nix { })
    (vscode-with-extensions.override {
      vscode = vscodium;
      vscodeExtensions =
        with vscode-extensions;
        [
          #bmewburn.vscode-intelephense-client
          #dracula-theme.theme-dracula
          mvllow.rose-pine
          editorconfig.editorconfig
          esbenp.prettier-vscode
          github.github-vscode-theme
          gruntfuggly.todo-tree
          jnoortheen.nix-ide
          mhutchie.git-graph
          ms-ceintl.vscode-language-pack-fr
          #junstyle.php-cs-fixer
          #ms-vscode-remote.remote-ssh
        ]
        ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
          {
            name = "vscode-twig-language-2";
            publisher = "mblode";
            version = "0.9.2";
            sha256 = "113w2iis4zi4z3sqc3vd2apyrh52hbh2gvmxjr5yvjpmrsksclbd";
          }
        ];
    })
    (chromium.override {
      enableWideVine = true; # DRM support
    })
    appimage-run
    audacious
    calibre
    digikam
    eaglemode
    filezilla
    gimp-with-plugins
    hunspell
    hunspellDicts.fr-moderne
    inkscape-with-extensions
    kitty
    latte-dock
    libreoffice-qt
    libsForQt5.ark
    mixxx
    mumble
    nextcloud-client
    obsidian
    signal-desktop
    thunderbird
    tigervnc
    transmission-qt
    unetbootin
    vlc

    ## niri scroll window manager
    niri
    waybar
    fuzzel
    alacritty

  ];

  # Do not restart the display manager automatically
  systemd.services.display-manager.restartIfChanged = lib.mkForce false;

  # If something needs more than 20s to stop it should probably be
  # killed.
  systemd.extraConfig = ''
    DefaultTimeoutStopSec=20s
  '';
}
