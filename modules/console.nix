# cli programs should be available on servers and desktop
{ config, pkgs, ... }:
let
  unstable =
    import
      (builtins.fetchTarball "https://github.com/nixos/nixpkgs/tarball/7afa2c97ebb0585d00c64c112344483e89e00545")
      # reuse the current configuration
      { config = config.nixpkgs.config; };
in
{
  programs.zsh.enable = true;
  programs.fzf = {
    fuzzyCompletion = true;
    keybindings = true;
  };

  # programs.neovim = {
  #   enable = true;
  #   defaultEditor = true;
  #   package = unstable.neovim;
  #   viAlias = true;
  #   vimAlias = true;
  # };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    ansible
    atuin
    bat
    btop
    cargo
    catnip
    cmatrix
    coreutils
    curl
    dnsutils
    emacs-nox
    fd
    findutils
    fzf
    gnugrep
    gnumake
    gnupg
    imagemagick
    jq
    git
    glances
    htop
    lazygit
    lsd
    magic-wormhole
    mc
    micro
    mosh
    mpc-cli
    mpd
    mpv
    neofetch
    unstable.neovim
    ollama
    pandoc
    pass
    pciutils
    pinentry
    ripgrep
    rustc
    slides
    starship
    syncthing
    tmux
    tokei
    tree
    unzip
    usbutils
    wget
    whois
    wl-clipboard
    wirelesstools
    yazi
    yt-dlp
    zellij
    zoxide
    zsh-autosuggestions
  ];
}
