{ pkgs, lib, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "mrflos";
  home.homeDirectory = "/home/${home.username}";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    #cli
    ansible
    bat
    btop
    catnip
    coreutils
    curl
    dnsutils
    emacs-nox
    fd
    findutils
    fzf
    gnugrep
    gnumake
    gnupg
    imagemagick
    jq
    git
    glances
    htop
    lazygit
    lsd
    mc
    micro
    mosh
    mpd
    neofetch
    nixfmt
    pandoc
    pass
    pciutils
    pinentry
    ripgrep
    slides
    starship
    tmux
    tree
    unzip
    usbutils
    wget
    whois
    wirelesstools
    yt-dlp
    zellij
    zola
    zsh-autosuggestions

    # apps
    kitty

    # fonts
    (pkgs.nerdfonts.override { fonts = [ "Iosevka" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/florian/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    EDITOR = "nvim";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  programs = {
    neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
    };
    starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        format = lib.concatStrings [
          "$\{custom.tztime\}"
          "$all"
          "$directory"
          "$character"
        ];
        add_newline = true; # Inserts a blank line between shell prompts
        character = {
          success_symbol = "[➜](bold green)";
          error_symbol = "[➜](bold red)";
        };
        custom.tztime = {
          command = "date +\"%a %d %b %H:%M%p\"";
          when = "true";
          format = "\\[\$symbol(\$output)\\] (purple)";
        };
      };
    };
    zsh = {
      enable = true;
      enableAutosuggestions = true;
      shellAliases = {
        ls = "lsd --hyperlink=auto";
        icat = "kitty +kitten icat";
        nixedit = "vi /etc/nixos";
        nixupdate = "sudo nix-channel --update && sudo nixos-rebuild switch";
        nixclean = "sudo nix-env --delete-generations old --profile /nix/var/nix/profiles/system && sudo /nix/var/nix/profiles/system/bin/switch-to-configuration switch && sudo nix-store --gc";
      };
      oh-my-zsh = {
         enable = true;
         plugins = [ "git" "ssh-agent" ];
         theme = "robbyrussell";
      };
    };
    ssh = {
      enable = true;
      extraConfig = ''
Host *
  IgnoreUnknown UseKeychain
  UseKeychain yes
  ServerAliveInterval 15
  ServerAliveCountMax 3

# CLIC mrflos rp4 Maison
Host mrflospw
HostName mrflos.pw
User mrflos
Port 4222
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

# CLIC Gatien
Host clicgatien
HostName clic.cooptic.be
Port 4222
User clic
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

# nas synology DS216play maison
Host nas
HostName 192.168.1.2
User mrflos
Port 65522
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

# nas laurent
Host naslaurent
HostName 82.65.9.168
Port 65522
User furax37
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

# ecocloud
Host ecocloud ecocloud-s-mart.im.utc 192.168.196.26
HostName 192.168.196.26
User optisseur
ProxyJump 10.200.0.240

# Friends
Host furax
User admin
HostName furax37.org
Port 22
IdentitiesOnly yes

# Yeswiki.pro
Host yeswikipro
HostName 138.201.86.17
Port 4222
User admin
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

# Proxmox Clic Yeswiki.pro
Host clicyeswikipro
HostName 136.243.103.121
Port 22
User root
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

# Bastion Globenet
Host bastionglobenet
HostName bastion.globenet.org
Port 20766
User yeswikipro
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

Host clicaudrey # ywp-clic-1 audrey
HostName 95.217.202.171
Port 22
User optisseur
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

Host ywp-clic-2
HostName 135.181.128.214
#Port 42022
Port 22
User optisseur
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519


# Chatons YesWiki
Host yeswiki
HostName yunohost.yeswiki.net
Port 6742
User mrflos
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

# YESWIKI
Host cooptools
User root
HostName ispconfig.coop.tools
Port 22
IdentitiesOnly yes
IdentityFile ~/.ssh/id_rsa

# COLIBRIS by server name

Host nascolibris
HostName 212.114.30.58
Port 42222
User backup
UseRoaming no
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

## Avant tchat mattermost / Apres piwigo - peertube - nextcloud - onlyoffice
Host emmagoldman
#HostName 2a01:4f8:190:7302::2
HostName 144.76.3.3
Port 22
User admin
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

## CLIC Yunohost colibox.colibris-outilslibres.org
Host georgesbrassens
HostName 65.21.50.40
Port 22
User admin
UseRoaming no
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

## Hebergements web sites colibris - LEMP ispconfig
Host ivanillitch
HostName 95.217.77.254
Port 4222
User admin
UseRoaming no
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

## Proxmox outils libres / jitsi - pads - mobilizon
Host leontolstoi
HostName 88.99.209.53
Port 22
User root
UseRoaming no
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

## BBB
Host louisemichel
HostName 88.99.95.38
Port 22
User root
UseRoaming no
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

## CLIC Yunohost clic-territoires.org interlab.cc
Host mikhailbakunin
HostName 78.46.65.157
Port 4222
User admin
IdentityFile ~/.ssh/id_ed25519
UseRoaming no
IdentitiesOnly yes

## Colibris.social semapps docker
Host murraybookchin
HostName 65.21.2.239
Port 4222
User admin
UseRoaming no
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

## Boutique Colibris - prestashop
Host pierreproudhon
HostName 176.9.59.51
Port 22
User admin
IdentityFile ~/.ssh/id_ed25519

## Gogocarto
Host rosaluxemburg
HostName 135.181.166.113
Port 4222
User admin
IdentityFile ~/.ssh/id_ed25519
UseRoaming no
IdentitiesOnly yes


# COLIBRIS colinames
Host colibbb
HostName 88.99.95.38
User root
IdentityFile ~/.ssh/id_ed25519

Host colitube
HostName video.colibris-outilslibres.org
Port 4222
User admin
IdentityFile ~/.ssh/id_ed25519

Host colipad
HostName pad.colibris-outilslibres.org
Port 4222
User admin
IdentityFile ~/.ssh/id_ed25519

Host colichat
HostName 144.76.3.3
Port 22001
User admin
IdentityFile ~/.ssh/id_ed25519

Host coliboutique
HostName 176.9.59.51
Port 22
User admin
IdentityFile ~/.ssh/id_ed25519

Host coliweb
HostName 95.217.77.254
Port 4222
User admin
IdentityFile ~/.ssh/id_ed25519

Host colicloud
HostName 135.181.166.113
Port 4222
User admin
IdentityFile ~/.ssh/id_ed25519

Host colicarto
HostName 135.181.166.113
Port 4222
User admin
IdentityFile ~/.ssh/id_ed25519

Host colihost
HostName postit.colibris-outilslibres.org
Port 22001
User admin
IdentityFile ~/.ssh/id_ed25519

# COLIBRIS
Host CartoDev
User admin
HostName carto-dev.colibris-outilslibres.org
Port 22102

Host hebergement
User admin
HostName hebergement.colibris-lemouvement.org
Port 4222
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

Host colibrisweb
HostName colibris.cc
Port 22
User admin
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519

Host colibrishost
HostName postit.colibris-outilslibres.org
Port 22001
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519
User admin

Host colibrispad
HostName pad.colibris-outilslibres.org
Port 22101
User admin

Host colibrispadpostgres
HostName pad.colibris-outilslibres.org
Port 22101
User postgres

Host colibrishostweb
HostName 144.76.3.3
Port 22001
User admin
IdentitiesOnly yes
IdentityFile ~/.ssh/id_ed25519


# framasoft
Host framagit.org
IdentityFile ~/.ssh/id_ed25519
User git
Port 22

# coopaname yeswiki test
Host coopaname
HostName 85.118.37.83
User florian
Port 51026
'';
    };
  };
}
