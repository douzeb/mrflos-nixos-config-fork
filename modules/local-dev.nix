# Local developpement
{
  pkgs,
  config,
  lib,
  ...
}:
{
  virtualisation = {
    docker.enable = true;
    #docker.rootless = {
    #  enable = true;
    #  setSocketVariable = true;
    #};
    #libvirtd.enable = true;
    #lxd.enable = true;
    #waydroid.enable = true;
  };

  systemd.services.yeswikidev = {
    enable = true;
    script = ''
      ${pkgs.systemd}/bin/systemctl start mysql
      # etc
    '';
    serviceConfig =
      let
        startstop =
          command:
          (pkgs.writeShellApplication {
            name = "yeswikidev";
            runtimeInputs = with pkgs; [ systemd ];
            text = ''
              systemctl ${command} mysql.service
            '';
          });
        #startstopFullShell = pkgs.writeShellApplication {
        #  name = "yeswikidev";
        #  runtimeInputs = with pkgs; [ systemd ];
        #  text = ''
        #    systemctl "$1" mysql.service
        #  '';
        #});
        #commands = [ "start" "stop" ];
        #scripts = map (command: (pkgs.writeShellApplication {
        #  name = "yeswikidev";
        #  runtimeInputs = with pkgs; [ systemd ];
        #  text = ''
        #    systemctl ${command} mysql.service
        #  '';
        #})) commands;

      in
      #execs = {
      #   ExecStart = "start";
      #   ExecStop = "stop";
      #};
      # https://nixos.org/manual/nix/stable/language/builtins
      # https://nixos.org/manual/nix/stable/language/builtins#builtins-mapAttrs
      #execAttrs = attrNames execs; # ["Start" "Stop"];
      #execs2 = mapAttrs (k: v: ()) execs;
      {
        # pkgs.writeScript, pkgs.writeScriptBin
        #ExecStart = startstop "start";
        #ExecStop = startstop "stop";
        # OR
        # ExecStart = "${startstopFullShell} start";
        # ExecStop  = "${startstopFullShell} stop";
        # OR
        # ExecStart = "${scripts[0]}";
        # ExecStop  = "${scripts[1]}";
      };
  };

  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages =
    with pkgs;
    let
      php83 = pkgs.php83.buildEnv { extraConfig = "memory_limit = 2G"; };
    in
    [
      #clang
      dart-sass
      dbeaver-bin
      docker-compose
      eris-go
      gcc
      go
      nixfmt-rfc-style
      lua
      lua-language-server
      luarocks
      nixpkgs-fmt
      nodejs
      nodePackages.eslint
      nodePackages.prettier
      php83
      php83Packages.composer
      php83Packages.php-cs-fixer
      python3
      python311Packages.virtualenv
      rpi-imager
      ruff-lsp
      stylelint
      symfony-cli
      tree-sitter
      virt-manager
      yarn
      zig
      # image optimizers
      image_optim
      nodePackages.svgo
      jpegoptim
      optipng
      pngquant
      gifsicle
      libwebp
    ];
}
